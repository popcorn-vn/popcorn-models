// Import the neccesary modules.
import mongoose from "mongoose";

// The anime schema used by mongoose.
const InvoiceSchema = new mongoose.Schema({
  ctime     : Date,
  utime     : Date,
  paid      : Boolean,
  amount    : Number,
  from      : Date,
  to        : Date,
  plan      : String,
  subscriptions   : [{
    type          : mongoose.Schema.ObjectId,
    ref           : 'User'
  }],
  user      : {
    type    : mongoose.Schema.ObjectId,
    ref     : 'User'
  }
});

InvoiceSchema.pre("save", function (next) {
  if (this.isNew == true) {
    this.ctime = Date.now()
  }

  this.utime = Date.now()
  next()
})

InvoiceSchema.pre("update", function() {
  this.update({},{ $set: { utime: Date.now() } })
})

// Create the anime model.
const Invoice = mongoose.model("Invoice", InvoiceSchema);

/**
 * A model object for Drive token.
 * @type {Drive}
 */
export default Invoice;
