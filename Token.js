// Import the neccesary modules.
import mongoose from "mongoose";

// The anime schema used by mongoose.
const TokenSchema = new mongoose.Schema({
  ctime           : Date,
  utime           : Date,
  name            : String,
  client_id       : String,     // Oauth2 id & secret to drive, onedrive, box.com
  client_secret   : String,
  scopes          : Array,
  access_token    : String,
  refresh_token   : String,
  expiry_date     : Number,
  cookies         : String,
  cookies_expiry  : Number,
  google_api_key  : String,     // Using for publis file in google drive
  google_cloud    : {
    path          : String,     // Path to service account credentials.
    bucketName    : String,
    bucketNameBridge: String,
    project_id    : String,
    type          : {
      type        : String     // To fix mongoose bug
    },
    private_key_id: String,
    private_key   : String,
    client_email  : String,
    client_id     : String,
    auth_uri      : String,
    token_uri     : String,
    auth_provider_x509_cert_url: String,
    client_x509_cert_url       : String
  },
  type            : Number,     // STORAGE_TYPE
  host            : Number,     // STORAGE_SERVER_TYPE
  email           : String,
  quota            : {
    total          : Number, // Total bytes
    used           : Number,
    free           : Number
  },
  maxFileSize      : Number,
  minFileSize      : Number,
  folderId         : {
    video          : String,
    image          : String,
    torrent        : String
  }
});

TokenSchema.pre("save", function (next) {
  if (this.isNew == true) {
    this.ctime = Date.now()
  }

  this.utime = Date.now()
  next()
})

TokenSchema.pre("update", function() {
  this.update({},{ $set: { utime: Date.now() } })
})

// Create the anime model.
const Token = mongoose.model("Token", TokenSchema);

/**
 * A model object for Token token.
 * @type {Token}
 */
export default Token;
