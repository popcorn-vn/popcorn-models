// Import the neccesary modules.
import mongoose from "mongoose";

// The show schema used by mongoose.
const ShowSchema = new mongoose.Schema({
  _id             : {
    type          : String,
    required      : true,
    index         : {
      unique      : true
    }
  },
  imdb_id           : String,
  tvdb_id           : String,
  title             : String,
  sub_title         : String,
  year              : String,
  slug              : String,
  synopsis          : String,
  runtime           : String,
  rating            : {
    percentage      : Number,
    watching        : Number,
    votes           : Number,
    loved           : Number,
    hated           : Number
  },
  country           : String,
  network           : String,
  air_day           : String,
  air_time          : String,
  status            : String,
  num_seasons       : Number,
  last_updated      : Number,
  last_checked    : Number,
  latest_episode    : {
    type            : Number,
    default         : 0
  },
  images            : {
    banner          : String,
    fanart          : String,
    poster          : String
  },
  genres            : [],
  source            : String,
  provider          : {
    type            : mongoose.Schema.ObjectId,
    ref             : 'User'
  },
  episodes          : [{
    season          : Number,
    episode         : Number,
    title           : String,
    overview        : String,
    first_aired     : Number,
    torrents        : {},
    source          : [{
      token         : {
        type        : mongoose.Schema.ObjectId,
        ref         : 'Token'
      },
      file_id       : String,
      quality     : String,
      others        : {}
    }],
    seeders       : [{
      quality     : String,
      token       : {
        type      : mongoose.Schema.ObjectId,
        ref       : 'Token'
      },
      file_id     : String,
      others      : {}
    }]
  }]
}, { usePushEach: true, minimize: false });

// Create the show model.
const Show = mongoose.model("Show", ShowSchema);

/**
 * A model object for shows.
 * @type {Show}
 */
export default Show;
