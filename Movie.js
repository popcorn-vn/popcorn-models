// Import the neccesary modules.
import mongoose from "mongoose";

// The movie schema used by mongoose.
const MovieSchema = new mongoose.Schema({
  _id             : {
    type          : String,
    required      : true,
    index         : {
      unique      : true
    }
  },
  imdb_id         : String,
  title           : String,
  sub_title       : {
    type          : String,
    default       : ""
  },
  year            : String,
  slug            : {
    type          : String,
    default       : ""
  },
  synopsis        : String,
  runtime         : String,
  rating          : {
    percentage    : Number,
    watching      : Number,
    votes         : Number,
    loved         : Number,
    hated         : Number
  },
  country         : {
    type          : String,
    default       : ""
  },
  last_updated    : {
    type          : Number,
    default       : Date.now()
  },
  last_checked    : Number,
  images          : {
    banner        : String,
    fanart        : String,
    poster        : String
  },
  genres          : [],
  released        : Number,
  trailer         : {
    type          : String,
    default       : null
  },
  certification   : String,
  torrents        : {},
  provider        : {
    type          : mongoose.Schema.ObjectId,
    ref           : 'User'
  },
  pictures        : {
    banner        : {
      token       : {
        type      : mongoose.Schema.ObjectId,
        ref       : 'Token'
      },
      file_id     : String
    },
    fanart        : {
      token       : {
        type      : mongoose.Schema.ObjectId,
        ref       : 'Token'
      },
      file_id     : String
    },
    poster        : {
      token       : {
        type      : mongoose.Schema.ObjectId,
        ref       : 'Token'
      },
      file_id     : String
    }
  },
  source          : [{
    token         : {
      type        : mongoose.Schema.ObjectId,
      ref         : 'Token'
    },
    file_id       : String,
    quality       : String,
    others        : {}
  }],
  seeders       : [{
    quality     : String,
    token       : {
      type      : mongoose.Schema.ObjectId,
      ref       : 'Token'
    },
    file_id     : String,
    others      : {}
  }],
  is_processing   : {
    type          : Boolean,
    default       : false
  }
}, { usePushEach: true, minimize: false });

// Create the movie model.
const Movie = mongoose.model("Movie", MovieSchema);

/**
 * A model object for movies.
 * @type {Movie}
 */
export default Movie;
