// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
const UserSchema = new Schema({
  ctime       : Date,
  utime       : Date,
  name        : String,
  email       : String,
  password    : String,
  website     : String,
  languages   : Array,
  status      : Number,
  role        : Number,
  balance     : Number,
  provider    : Boolean,
  statistics  : {
    animes    : Number,
    movies    : Number,
    shows     : Number
  },
  plan        : {          // USER_PLAN
    StorageFree : Number,
    ReUploadFree: Number,
  },
  setting       : {
    staticUrl   : Boolean,
    lang        : String,
  }
});

UserSchema.pre("save", function (next) {
  if (this.isNew == true) {
    this.ctime = Date.now()
  }

  this.utime = Date.now()
  next()
})

UserSchema.pre("update", function() {
  this.update({},{ $set: { utime: Date.now() } })
})

// Create the User model.
const User = mongoose.model("User", UserSchema);
export default User;
