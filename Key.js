// Import the neccesary modules.
import mongoose from "mongoose";

const KeySchema = new mongoose.Schema({
  apiKey          : String,
  key             : String,
  iv              : String,
  ctime           : Date,
  utime           : Date,
  owner           : {
    type          : mongoose.Schema.ObjectId,
    ref           : 'User'
  },
  active          : {
    type          : Boolean,
    default       : false
  },
  subscriptions   : [{
    type          : mongoose.Schema.ObjectId,
    ref           : 'User'
  }],
  quota           : {
    total         : Number, // Total request/month
    usedToday     : Number,
    usedYes       : Number,
    usedMon       : Number,
    free          : Number
  }
});

KeySchema.pre("save", function (next) {
  if (this.isNew == true) {
    this.ctime = Date.now()
  }

  this.utime = Date.now()
  next()
})

KeySchema.pre("update", function() {
  this.update({},{ $set: { utime: Date.now() } })
})

// Create the anime model.
const Key = mongoose.model("Key", KeySchema);

/**
 * A model object for Token token.
 * @type {Token}
 */
export default Key;
