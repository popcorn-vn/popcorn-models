// Import the neccesary modules.
import mongoose from "mongoose";

// The anime schema used by mongoose.
const AnimeSchema = new mongoose.Schema({
  mal_id          : {
    type          : String,
    required      : true,
    index         : {
      unique      : true
    }
  },
  title           : String,
  sub_title       : String,
  year            : String,
  slug            : String,
  synopsis        : String,
  runtime         : String,
  status          : String,
  rating          : {
    percentage    : Number,
    watching      : Number,
    votes         : Number,
    loved         : Number,
    hated         : Number
  },
  type            : String,
  num_seasons     : Number,
  seasons         : [{
    name          : String,
    mal_id        : String
  }],
  last_updated    : Number,
  last_checked    : Number,
  latest_episode  : {
    type          : Number,
    default       : 0
  },
  images          : {
    banner        : String,
    fanart        : String,
    poster        : String
  },
  provider        : {
    type          : mongoose.Schema.ObjectId,
    ref           : 'User'
  },
  pictures        : {
    banner        : {
      token       : {
        type      : mongoose.Schema.ObjectId,
        ref       : 'Token'
      },
      file_id     : String
    },
    fanart        : {
      token       : {
        type      : mongoose.Schema.ObjectId,
        ref       : 'Token'
      },
      file_id     : String
    },
    poster        : {
      token       : {
        type      : mongoose.Schema.ObjectId,
        ref       : 'Token'
      },
      file_id     : String
    }
  },
  genres          : [],
  episodes        : [{
    season        : String,
    episode       : String,
    title         : String,
    overview      : String,
    first_aired   : Number,
    tvdb_id       : String,
    torrents      : {},
    dottorrent    : [{
      quality     : String,
      token       : {
        type      : mongoose.Schema.ObjectId,
        ref       : 'Token'
      },
      file_id     : String
    }],
    source        : [{
      quality     : String,
      token       : {
        type      : mongoose.Schema.ObjectId,
        ref       : 'Token'
      },
      file_id     : String,
      others      : {}
    }],
    seeders       : [{
      quality     : String,
      token       : {
        type      : mongoose.Schema.ObjectId,
        ref       : 'Token'
      },
      file_id     : String,
      others      : {}
    }]
  }],
  trailer         : {
    type          : String,
    default       : null
  },
  animet          : {
    id            : String,
    url           : String,
    first_chap    : String,
    chaps         : []
  },
  country         : String,
  air_day         : String,
  air_time        : String,
  is_processing   : { // is clone file to storage processing this file or scraper tool is processing
    type          : Boolean,
    default       : false
  }
}, { usePushEach: true, minimize: false });

// Create the anime model.
const Anime = mongoose.model("Anime", AnimeSchema);

/**
 * A model object for anime shows.
 * @type {Anime}
 */
export default Anime;
